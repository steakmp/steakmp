/**
 * Provides a simple word object that stores a word together with its letters
 * sorted.
 *
 * @author Jennifer Salinas
 * @version 11-18-18
 *
 *
 */
import java.util.Arrays;

public class Word
{
	private String word;    // the word
	private String sorted;  // the word re-arranged so that its letters are sorted

	/**
	 * Store the word and create a sorted version of the word.
	 *
	 * @param word the word to be stored
	 */
	public Word(String word)
	{
		char [] letters= word.toCharArray();
		Arrays.sort(letters);
		this.word= word;
		this.sorted= new String(letters);
	}
   
   
   
   
   public boolean anaWord(Word other)
   {
      char [] otherletters= other.word.toCharArray();
		Arrays.sort(otherletters);  
      String sortedOther = new String(otherletters);
      if(sorted==sortedOther)
      {
         return true;
      }
      else
      {
      return false;
      }
   }
   



	/**
	 * Just show the regular word when object is printed.
	 *
	 * @return the regular word
	 */
	public String toString()
	{
		return this.word + "(" + sorted + ")";
	}
   
   
   @Override
   public boolean equals(Object obj)
   {
      if(this!=obj)
      {
         return false;
      }
   
      if (obj == null) 
      {
         return false;
      }

      if(!(obj instanceof Word))
      {
         return false;
      }
      return true;
   }
   
   
   @Override
   public int hashCode() 
   {
      return this.word.hashCode();
   
   }
   
   
}


