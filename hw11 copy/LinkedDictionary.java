/**
 * Linked Dictionary.
 *
 * @author Jennifer Salinas
 * @version 11-18-18
 *
 */


import java.io.*;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.Scanner;


public class LinkedDictionary extends WordList
{
   private WordNode head;
   String wordFile = "..data/mywords.txt";
   //String wordList;
   
   
   
   public LinkedDictionary()
   {
      super();
   }
   


	/**
	 * Read words from file and insert into list.
	 *
	 * @param String wordFile
	 */  
   public String LinkedDictionary(String wordFile)
   {
      try
      {
         Scanner file=new Scanner(new FileInputStream(wordFile));
         while(file.hasNextLine())
         {
            Word wordList = new Word(file.nextLine());
            insert(wordList);
         }  
         file.close();
      }            
      catch (Exception e){}
   }
   
   
   
   
   /**
	 * Read words from file and insert into list.
	 *
	 * @param String wordFile
	 */   
   public int countAnagrams(Word gram)
   {
      int num=0;
      try
      {
         Scanner file=new Scanner(new FileInputStream(wordFile));
         while(file.hasNextLine())
         {
            num++;
         }  
         file.close();
      }            
      catch (Exception e){}
      return num;
   }
   
   
  /* 
   public void displayBigAnagramFamilies()
   {
   
   }
 
   */
   
}