/**
 * A linked list node holding word data.
 *
 * @author Jennifer Salinas
 * @version 11-18-18
 *
 */
public class WordNode
{
	String data;
	WordNode next;
}
