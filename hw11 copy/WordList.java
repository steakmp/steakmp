/**
 * A linked list of word values.
 *
 * @author Jennifer Salinas
 * @version 11-18-18
 *
 */
 

public class WordList
{
	private WordNode head;
   private String data;

	/**
	 * A new list has head pointing nowhere.
	 */
	public WordList()
	{
		head= null;
	}


	/**
	 * Displays contents of the list.
	 */
	public void display()
	{
      WordNode a=head;
      while(a!=null)
      {
         System.out.println(a.data);
         a=a.next;
      }

	}


	/**
	 * In an unordered list we can just add to the front.
	 *
	 * @param newword The new element to be inserted into the list.
	 */
	public void insert(String newword)
	{
      WordNode a=new WordNode();
      a.data=newword;
      a.next=head;
      head=a;
   }


	/**
	 * Search the list for the value val.
	 *
	 * @param val the value to be searched for
	 * @return reference to the found node (null if not found)
	 */
	public WordNode search(String val)
   { 
      WordNode a = head; 
      while (a!= null && a.data!=val) 
      { 
         a=a.next;     
      }
      return a.data;
   } 
   

	/**
	 * Find first occurrence of val (if it exists) and remove it from the list.
	 *
	 * @param val the value to be removed
	 */
	public void remove(String val)
	{
      WordNode p, q;
      q=null;
      p=head;
      while(p!=null && !(p.data.equals(val)))
      {
         q=p;
         p=p.next;
      }
      if(p!=null && q==null)
      {
         if(p.next!=null)
         {
            head=p.next;
         }
         else
         {
            head=null;
         }
      }
      else if(p!=null)
      {
         q.next=p.next;
      }   
   }
}
