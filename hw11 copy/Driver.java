/**
 * Driver for testing WordList code.
 *
 * @author Jennifer Salinas
 * @version 11-18-18
 *
 */
 
import java.io.BufferedReader;
import java.io.FileReader; 
import java.io.IOException;

public class Driver
{
	public static void main(String [] args)
	{
      LinkedDictionary word= new LinkedDictionary();
      word.display();
   }

}

