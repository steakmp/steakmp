/**
 * File of 2D Array of Characters.
 *
 * @author  Jennifer Salinas
 * @version 11-04-18
 *
 * <pre>
 * The code to read and display array of 2D chars in a file.
 * </pre>
*/

import java.io.*;
import java.util.*;


public class MyCanvas
{
   protected int rows;
   protected int col;
   protected char border;
   protected char [][] c;
 
   
  /**
   * Load image file by converting from string into char 2D array.
   *
   * @param String dataFile
   *
  */   
   public void load(String dataFile)
   {
      try
      {
         Scanner file = new Scanner (new File(dataFile));
         String line;
         rows=file.nextInt();
         col=file.nextInt();
         c=new char[rows][col];
         
         for(int i=0; i<rows; i++)  
         {         
            line=file.nextLine();
            for(int j=0; j<col; j++)
            {
               c[i][j]=line.charAt(j);
            }
         }
         file.close();
      }
      catch (Exception e) {}
   }
                                 
 
   
  /**
   * Displays image file of char 2D array.
   *
  */   
   
   public void show()
   {
      int i,j;

      for(i=0; i<c.length; i++)
      {
         for(j=0; j<c.length; j++);
         {
            System.out.print(c[i][j]);
         }
         System.out.println();
      }
   }
   



}