/**
 * Testing Run argument line
 *
 * @author Jennifer Salinas
 * @version 10-16-18
 *
*/ 
   
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;


public class Palindrome
{
   protected String filename;
   protected CodeTimer timer;
   protected int numWords;
   protected int palWords;
   protected double percentWords;

   public Palindrome(String filename)
   {
      timer=new CodeTimer();
      numWords = 0;
      palWords = 0;
      this.filename = filename;
   
   }
      
	public void PalindromeWords()
   {
      try
      {
         timer.start();
         //System.out.println("Total number of params: "+ args.length);
         BufferedReader testfile=new BufferedReader(new FileReader("test.txt"));
         List<String>totalLines = new ArrayList<String>();
         String line = null;
         //String []array;
         int n = 0;
         while((line = testfile.readLine())!=null)
         {
      /*    if(n<=file.length)
            {
               args[n]=line;
               n++;
            }
            System.out.println(str);
      */    
            totalLines.add(line);
         
            if(isPalindrome(line))
            {
               numWords++;
               palWords++;
              // System.out.println(line + " is a palindrome");
            }
            else
            {
               numWords++;
               System.out.println(line + " is not a palindrome");
            }
         }
         testfile.close();
         timer.stop();
         percentWords = ((palWords/numWords)%100);
         
         System.out.println("+--------------------+");
         System.out.println("Input file         : "+filename);
         System.out.println("IWords Processed   : "+numWords);
         System.out.println("# of Palindromes   : "+palWords);
         System.out.println("% of Palindromes   : "+percentWords);
         //System.out.printf("%.0d", percentWords);
         System.out.println("Time Elapsed       : "+timer);
         //System.out.println("% of Palindromes   : "+percentWords);
         //System.out.printf("%.0d", percentWords);
         System.out.println("+--------------------+");
      }
      catch(Exception e){}
   }
   
      
   
   
   public static boolean isPalindrome(String line)
   {
      if(line.length() == 0 || line.length()==1)
      {
         //System.out.println("true");
         return true;
      }

      if(line.charAt(0) == line.charAt(line.length()-1))
      {
         return isPalindrome(line.substring(1, line.length()-1));
         //System.out.println("true");

      }
      //System.out.println("false");
      return false;

   }
   
}

